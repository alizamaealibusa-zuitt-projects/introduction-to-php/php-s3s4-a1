<?php require "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP OOP | Activity</title>
</head>
<body>

	<ul>
		<li><?php echo $newProduct->printDetails(); ?></li>
		<li><?php echo $newMobile->printDetails(); ?></li>
		<li><?php echo $newComputer->printDetails(); ?></li>
	</ul>


	<?php $newProduct->setInStock(3); ?>
    <?php $newMobile->setInStock(5); ?>
    <?php $newComputer->setCategory("laptops, computers and electronics"); ?>

    <ul>
    	<li>
    		Updated stock number of product object: <?php echo $newProduct->getInStock(); ?>
    	</li>
    	<li>
    		Updated stock number of mobile object: <?php echo $newMobile->getInStock(); ?>
    	</li>
    	<li>
    		Updated category of computer object: <?php echo $newComputer->getCategory(); ?>
    	</li>
    </ul>

</body>
</html>